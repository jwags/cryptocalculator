package com.jordansportfolio.cryptocalculator.Helper;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;

import com.jordansportfolio.cryptocalculator.DAO.CryptoDAO;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by WAGNERJ5 on 12/20/2017.
 */

public class CryptoHelper {
    private static final String preApiUrl = "https://api.coinbase.com/v2/prices/";
    private static final String postApiUrl = "-USD/spot";

    private static BigDecimal getCurrentPriceFromAPIForCryptoAbbreviation(String abbreviation) {
        BigDecimal price = new BigDecimal(0);
        String amount = "0";

        try {
            CryptoApi cryptoApi = new CryptoApi();
            String result = cryptoApi.execute(preApiUrl + abbreviation + postApiUrl).get();

            JSONObject json = new JSONObject(result);
            amount = json.getJSONObject("data").getString("amount");
            /*DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(result));

            Document doc = builder.parse(src);
            amount = doc.getElementsByTagName("amount").item(0).getTextContent();*/

        } catch(Exception e) {
            Log.e("Exception on helper", e.toString());
        }

        return new BigDecimal(amount);
    }

    private static BigDecimal getPriceOfBitcoinByAbbreviation() {
        return getCurrentPriceFromAPIForCryptoAbbreviation("BTC");
    }
    private static BigDecimal getPriceOfLitecoinByAbbreviation() {
        return getCurrentPriceFromAPIForCryptoAbbreviation("LTC");
    }
    private static BigDecimal getPriceOfEthereumByAbbreviation() {
        return getCurrentPriceFromAPIForCryptoAbbreviation("ETH");
    }
    private static BigDecimal getPriceOfBitcoinCashByAbbreviation() {
        return getCurrentPriceFromAPIForCryptoAbbreviation("BCH");
    }



    public static String getValueOfAmountOwnedForBitcoin(Context ctx) {
        CryptoDbHelper cryptoDb = new CryptoDbHelper(ctx);

        BigDecimal priceOfBitcoin = CryptoHelper.getPriceOfBitcoinByAbbreviation();
        BigDecimal amountOfBitcoinOwned = cryptoDb.getBitcoinTotal();
        BigDecimal totalOwnedPriceOfBitcoin = priceOfBitcoin.multiply(amountOfBitcoinOwned).setScale(2, RoundingMode.HALF_EVEN);

        return totalOwnedPriceOfBitcoin.toString();
    }

    public static String getValueOfAmountOwnedForLitecoin(Context ctx) {
        CryptoDbHelper cryptoDb = new CryptoDbHelper(ctx);

        BigDecimal priceOfLitecoin = CryptoHelper.getPriceOfLitecoinByAbbreviation();
        BigDecimal amountOfLitecoinOwned = cryptoDb.getLitecoinTotal();
        BigDecimal totalOwnedPriceOfLitecoin = priceOfLitecoin.multiply(amountOfLitecoinOwned).setScale(2, RoundingMode.HALF_EVEN);

        return totalOwnedPriceOfLitecoin.toString();
    }

    public static String getValueOfAmountOwnedForEthereum(Context ctx) {
        CryptoDbHelper cryptoDb = new CryptoDbHelper(ctx);

        BigDecimal priceOfEthereum = CryptoHelper.getPriceOfEthereumByAbbreviation();
        BigDecimal amountOfEthereumOwned = cryptoDb.getEthereumTotal();
        BigDecimal totalOwnedPriceOfEthereum = priceOfEthereum.multiply(amountOfEthereumOwned).setScale(2, RoundingMode.HALF_EVEN);

        return totalOwnedPriceOfEthereum.toString();
    }

    public static String getValueOfAmountOwnedForBitcoinCash(Context ctx) {
        CryptoDbHelper cryptoDb = new CryptoDbHelper(ctx);

        BigDecimal priceOfBitcoinCash = CryptoHelper.getPriceOfBitcoinCashByAbbreviation();
        BigDecimal amountOfBitcoinCashOwned = cryptoDb.getBitcoinCashTotal();
        BigDecimal totalOwnedPriceOfBitcoinCash = priceOfBitcoinCash.multiply(amountOfBitcoinCashOwned).setScale(2, RoundingMode.HALF_EVEN);

        return totalOwnedPriceOfBitcoinCash.toString();
    }
}
