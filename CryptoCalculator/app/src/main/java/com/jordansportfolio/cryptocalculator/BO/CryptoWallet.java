package com.jordansportfolio.cryptocalculator.BO;

import java.math.BigDecimal;

/**
 * Created by WAGNERJ5 on 12/20/2017.
 */

public class CryptoWallet extends CryptoType {
    public int CryptoWalletID;
    public BigDecimal AmountOwned;


    public int getCryptoWalletID() {
        return CryptoWalletID;
    }
    public void setCryptoWalletID(int cryptoWalletID) {
        CryptoWalletID = cryptoWalletID;
    }

    public BigDecimal getAmountOwned() {
        return AmountOwned;
    }
    public void setAmountOwned(BigDecimal amountOwned) {
        AmountOwned = amountOwned;
    }
}
