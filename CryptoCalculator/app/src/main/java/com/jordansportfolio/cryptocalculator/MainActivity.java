package com.jordansportfolio.cryptocalculator;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.jordansportfolio.cryptocalculator.BO.CryptoType;
import com.jordansportfolio.cryptocalculator.DAO.CryptoDAO;
import com.jordansportfolio.cryptocalculator.Helper.CryptoDbHelper;
import com.jordansportfolio.cryptocalculator.Helper.CryptoHelper;

import java.math.RoundingMode;
import java.util.ArrayList;

import static com.jordansportfolio.cryptocalculator.Helper.CryptoHelper.getValueOfAmountOwnedForLitecoin;
import static com.jordansportfolio.cryptocalculator.R.id.litecoin_value;

public class MainActivity extends AppCompatActivity {

    CryptoDbHelper Db;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Db = new CryptoDbHelper(this);


        TextView bitcoin_value = (TextView) findViewById(R.id.bitcoin_value);
        String bitcoinValue = CryptoHelper.getValueOfAmountOwnedForBitcoin(this);
        bitcoin_value.setText("$" + bitcoinValue);

        TextView litecoin_value = (TextView) findViewById(R.id.litecoin_value);
        String litecoinValue = CryptoHelper.getValueOfAmountOwnedForLitecoin(this);
        litecoin_value.setText("$" + litecoinValue);

        TextView ethereum_value = (TextView) findViewById(R.id.ethereum_value);
        String ethereumValue = CryptoHelper.getValueOfAmountOwnedForEthereum(this);
        ethereum_value.setText("$" + ethereumValue);

        TextView bitcoin_cash_value = (TextView) findViewById(R.id.bitcoin_cash_value);
        String bitcoinCashValue = CryptoHelper.getValueOfAmountOwnedForBitcoinCash(this);
        bitcoin_cash_value.setText("$" + bitcoinCashValue);



        Button bitcoinButton = (Button) findViewById(R.id.btn_bitcoin);
        bitcoinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, BitcoinActivity.class));
            }
        });
        Button ethereumButton = (Button) findViewById(R.id.btn_ethereum);
        ethereumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, EthereumActivity.class));
            }
        });
        Button litecoinButton = (Button) findViewById(R.id.btn_litecoin);
        litecoinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LitecoinActivity.class));
            }
        });
        Button bitcoinCashButton = (Button) findViewById(R.id.btn_bitcoin_cash);
        bitcoinCashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, BitcoinCashActivity.class));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                return true;
            case R.id.menu_bitcoin:
                startActivity(new Intent(MainActivity.this, BitcoinActivity.class));
                return true;
            case R.id.menu_bitcoin_cash:
                startActivity(new Intent(MainActivity.this, BitcoinCashActivity.class));
                return true;
            case R.id.menu_ethereum:
                startActivity(new Intent(MainActivity.this, EthereumActivity.class));
                return true;
            case R.id.menu_litecoin:
                startActivity(new Intent(MainActivity.this, LitecoinActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
