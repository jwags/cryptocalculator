package com.jordansportfolio.cryptocalculator.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jordansportfolio.cryptocalculator.BO.CryptoType;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by WAGNERJ5 on 12/20/2017.
 */

public class CryptoDbHelper extends SQLiteOpenHelper {
    private static final String CRYPTO_WALLET_CREATE_TABLE =
            "create table CRYPTO_WALLET("
                    + "ID integer primary key, "
                    + "CRYPTO_TYPE_ID integer NOT NULL unique, "
                    + "AMOUNT_OWNED TEXT NOT NULL, "
                    + "FOREIGN KEY (CRYPTO_TYPE_ID) REFERENCES CRYPTO_TYPES (ID)"
                    +");";
    private static final String CRYPTO_TYPES_CREATE_TABLE =
            "create table CRYPTO_TYPES("
                    + "ID integer primary key, "
                    + "ABBREVIATION text NOT NULL, "
                    + "NAME text NOT NULL"
                    + ");";
    private static final String INSERT_DEFAULT_CRYPTO_TYPES =
            "insert into CRYPTO_TYPES ("
                    + "ID, "
                    + "ABBREVIATION, "
                    + "NAME"
                    + ") VALUES "
                    + "(1, 'BTC', 'BITCOIN'), "
                    + "(2, 'BCH', 'BITCOIN CASH'), "
                    + "(3, 'ETH', 'ETHEREUM'), "
                    + "(4, 'LTC', 'LITECOIN');";
    private static final String DROP_CRYPTO_TYPES = "DROP TABLE IF EXISTS CRYPTO_TYPES;";
    private static final String DROP_CRYPTO_WALLET = "DROP TABLE IF EXISTS CRYPTO_WALLET; ";
    private static final String DATABASE_NAME = "CRYPTO_DB";
    private static final String DATABASE_TABLE = "CRYPTO_TYPES";
    private static final int DATABASE_VERSION = 4;
    private SQLiteDatabase db;

    public CryptoDbHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CRYPTO_TYPES_CREATE_TABLE);
        db.execSQL(CRYPTO_WALLET_CREATE_TABLE);
        db.execSQL(INSERT_DEFAULT_CRYPTO_TYPES);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_CRYPTO_WALLET);
        db.execSQL(DROP_CRYPTO_TYPES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
















    //============================================================================
    public ArrayList<CryptoType> getAllCryptoTypes() {
        ArrayList<CryptoType> cryptoTypeList = new ArrayList<CryptoType>();
        CryptoType cryptoType = new CryptoType();
        db = this.getWritableDatabase();

        try {
            Cursor c = db.query("CRYPTO_TYPES", new String[] {
                    "ID", "ABBREVIATION", "NAME"}, null, null, null, null, null);

            int numRows = c.getCount();
            c.moveToFirst();
            for (int i = 0; i < numRows; ++i) {
                cryptoType = new CryptoType();
                cryptoType.CryptoTypeID = c.getInt(0);
                cryptoType.Abreviation = c.getString(1);
                cryptoType.Name = c.getString(2);

                cryptoTypeList.add(cryptoType);
                c.moveToNext();
            }
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }

        return cryptoTypeList;
    }

    private int getCryptoTypeIDByCryptoAbbreviation(String abbreviation) {
        db = this.getWritableDatabase();
        int ID = 0;

        try {
            Cursor c = db.query("CRYPTO_TYPES", new String[] {
                    "ID"}, "ABBREVIATION=?", new String[] { abbreviation }, null, null, null);

            c.moveToFirst();
            ID = c.getInt(0);
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }

        return ID;
    }

    private BigDecimal getTotalByCryptoAbbreviation(String abbreviation) {
        db = this.getWritableDatabase();
        BigDecimal total = new BigDecimal(0);
        int CryptoTypeID = getCryptoTypeIDByCryptoAbbreviation(abbreviation);

        try {
            Cursor c = db.query("CRYPTO_WALLET", new String[] {
                    "AMOUNT_OWNED"}, "CRYPTO_TYPE_ID=?", new String[] { String.valueOf(CryptoTypeID) }, null, null, null);

            int numRows = c.getCount();
            if(numRows > 0) {
                c.moveToFirst();
                total = new BigDecimal(c.getString(0));
            }
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }

        return total;
    }

    public BigDecimal getBitcoinTotal() {
        return getTotalByCryptoAbbreviation("BTC");
    }
    public BigDecimal getLitecoinTotal() {
        return getTotalByCryptoAbbreviation("LTC");
    }
    public BigDecimal getEthereumTotal() {
        return getTotalByCryptoAbbreviation("ETH");
    }
    public BigDecimal getBitcoinCashTotal() {
        return getTotalByCryptoAbbreviation("BCH");
    }


    private BigDecimal addAmountOwnedByCryptoAbbreviation(String abbreviation, BigDecimal amount) {
        db = this.getWritableDatabase();
        BigDecimal total = getTotalByCryptoAbbreviation(abbreviation).add(amount);
        int CryptoTypeID = getCryptoTypeIDByCryptoAbbreviation(abbreviation);

        try {
            ContentValues values = new ContentValues();
            values.put("AMOUNT_OWNED", total.toString());
            values.put("CRYPTO_TYPE_ID", CryptoTypeID);

            int id = (int) db.insertWithOnConflict("CRYPTO_WALLET", null, values, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                db.update("CRYPTO_WALLET", values, "CRYPTO_TYPE_ID=?", new String[]{String.valueOf(CryptoTypeID)});
            }
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }

        return getTotalByCryptoAbbreviation(abbreviation);
    }

    public BigDecimal addBitcoinAmount(BigDecimal amount) {
        return addAmountOwnedByCryptoAbbreviation("BTC", amount);
    }
    public BigDecimal addLitecoinAmount(BigDecimal amount) {
        return addAmountOwnedByCryptoAbbreviation("LTC", amount);
    }
    public BigDecimal addEthereumAmount(BigDecimal amount) {
        return addAmountOwnedByCryptoAbbreviation("ETH", amount);
    }
    public BigDecimal addBitcoinCashAmount(BigDecimal amount) {
        return addAmountOwnedByCryptoAbbreviation("BCH", amount);
    }


    private BigDecimal subtractAmountOwnedByCryptoAbbreviation(String abbreviation, BigDecimal amount) {
        db = this.getWritableDatabase();
        BigDecimal total = getTotalByCryptoAbbreviation(abbreviation).subtract(amount);
        int CryptoTypeID = getCryptoTypeIDByCryptoAbbreviation(abbreviation);

        try {
            ContentValues values = new ContentValues();
            values.put("AMOUNT_OWNED", total.toString());
            values.put("CRYPTO_TYPE_ID", CryptoTypeID);

            int id = (int) db.insertWithOnConflict("CRYPTO_WALLET", null, values, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                db.update("CRYPTO_WALLET", values, "CRYPTO_TYPE_ID=?", new String[]{String.valueOf(CryptoTypeID)});
            }
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }

        return getTotalByCryptoAbbreviation(abbreviation);
    }

    public BigDecimal subtractBitcoinAmount(BigDecimal amount) {
        return subtractAmountOwnedByCryptoAbbreviation("BTC", amount);
    }
    public BigDecimal subtractLitecoinAmount(BigDecimal amount) {
        return subtractAmountOwnedByCryptoAbbreviation("LTC", amount);
    }
    public BigDecimal subtractEthereumAmount(BigDecimal amount) {
        return subtractAmountOwnedByCryptoAbbreviation("ETH", amount);
    }
    public BigDecimal subtractBitcoinCashAmount(BigDecimal amount) {
        return subtractAmountOwnedByCryptoAbbreviation("BCH", amount);
    }


    private BigDecimal setAmountOwnedByCryptoAbbreviation(String abbreviation, BigDecimal amount) {
        db = this.getWritableDatabase();
        int CryptoTypeID = getCryptoTypeIDByCryptoAbbreviation(abbreviation);

        try {
            ContentValues values = new ContentValues();
            values.put("AMOUNT_OWNED", amount.toString());
            values.put("CRYPTO_TYPE_ID", CryptoTypeID);

            int id = (int) db.insertWithOnConflict("CRYPTO_WALLET", null, values, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                db.update("CRYPTO_WALLET", values, "CRYPTO_TYPE_ID=?", new String[]{String.valueOf(CryptoTypeID)});
            }
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }

        return getTotalByCryptoAbbreviation(abbreviation);
    }

    public BigDecimal setBitcoinAmount(BigDecimal amount) {
        return setAmountOwnedByCryptoAbbreviation("BTC", amount);
    }
    public BigDecimal setLitecoinAmount(BigDecimal amount) {
        return setAmountOwnedByCryptoAbbreviation("LTC", amount);
    }
    public BigDecimal setEthereumAmount(BigDecimal amount) {
        return setAmountOwnedByCryptoAbbreviation("ETH", amount);
    }
    public BigDecimal setBitcoinCashAmount(BigDecimal amount) {
        return setAmountOwnedByCryptoAbbreviation("BCH", amount);
    }



    public void close() {
        db.close();
    }
}
