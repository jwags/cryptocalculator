package com.jordansportfolio.cryptocalculator.BO;

/**
 * Created by WAGNERJ5 on 12/20/2017.
 */

public class CryptoType {
    public int CryptoTypeID;
    public String Abreviation;
    public String Name;


    public int getCryptoTypeID() {
        return CryptoTypeID;
    }
    public void setCryptoTypeID(int cryptoTypeID) {
        CryptoTypeID = cryptoTypeID;
    }

    public String getAbreviation() {
        return Abreviation;
    }
    public void setAbreviation(String abreviation) {
        Abreviation = abreviation;
    }
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }
}
