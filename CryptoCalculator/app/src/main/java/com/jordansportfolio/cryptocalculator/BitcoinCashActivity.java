package com.jordansportfolio.cryptocalculator;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jordansportfolio.cryptocalculator.DAO.CryptoDAO;
import com.jordansportfolio.cryptocalculator.Helper.CryptoDbHelper;
import com.jordansportfolio.cryptocalculator.Helper.CryptoHelper;

import java.math.BigDecimal;

import static com.jordansportfolio.cryptocalculator.Helper.CryptoHelper.getValueOfAmountOwnedForBitcoin;
import static com.jordansportfolio.cryptocalculator.Helper.CryptoHelper.getValueOfAmountOwnedForBitcoinCash;

public class BitcoinCashActivity extends AppCompatActivity {

    private Context ctx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CryptoDbHelper cryptoDb = new CryptoDbHelper(this);
        ctx = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitcoin_cash);


        TextView amtOwnedObj = (TextView) findViewById(R.id.amt_owned);
        String amtOwned = cryptoDb.getBitcoinCashTotal().toString();
        amtOwnedObj.setText(amtOwned);

        TextView valueObj = (TextView) findViewById(R.id.value);
        String value = getValueOfAmountOwnedForBitcoinCash(this);

        if(value == "" || value == "0") {
            value = "0.00";
        } else if(value.charAt(0) == '.') {
            value = "0" + value;
        }


        valueObj.setText("$" + value);




        Button addButton = (Button) findViewById(R.id.btn_bought);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CryptoDbHelper cryptoDb = new CryptoDbHelper(ctx);

                TextView amtOwnedObj = (TextView) findViewById(R.id.amt_owned);
                TextView updatedAmtObj = (TextView) findViewById(R.id.update_amt);
                String updatedAmount = updatedAmtObj.getText().toString();

                if(updatedAmount == null || updatedAmount == "") {
                    updatedAmount = "0.00";
                } else if(updatedAmount.charAt(0) == '.') {
                    updatedAmount = "0" + updatedAmount;
                }

                if(updatedAmount.matches("\\d+(?:\\.\\d+)?")) {
                    BigDecimal updatedAmt = new BigDecimal(updatedAmount);
                    amtOwnedObj.setText(cryptoDb.addBitcoinCashAmount(updatedAmt).toString());

                    TextView valueOfAmtOwnedObj = (TextView) findViewById(R.id.value);
                    valueOfAmtOwnedObj.setText("$" + getValueOfAmountOwnedForBitcoinCash(ctx));
                }
            }
        });
        Button subtractButton = (Button) findViewById(R.id.btn_sold);
        subtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CryptoDbHelper cryptoDb = new CryptoDbHelper(ctx);

                TextView amtOwnedObj = (TextView) findViewById(R.id.amt_owned);
                TextView updatedAmtObj = (TextView) findViewById(R.id.update_amt);
                String updatedAmount = updatedAmtObj.getText().toString();

                if(updatedAmount == null || updatedAmount == "") {
                    updatedAmount = "0.00";
                } else if(updatedAmount.charAt(0) == '.') {
                    updatedAmount = "0" + updatedAmount;
                }

                if(updatedAmount.matches("\\d+(?:\\.\\d+)?")) {
                    BigDecimal updatedAmt = new BigDecimal(updatedAmount);
                    amtOwnedObj.setText(cryptoDb.subtractBitcoinCashAmount(updatedAmt).toString());

                    TextView valueOfAmtOwnedObj = (TextView) findViewById(R.id.value);
                    valueOfAmtOwnedObj.setText("$" + getValueOfAmountOwnedForBitcoinCash(ctx));
                }
            }
        });
        Button setButton = (Button) findViewById(R.id.btn_set);
        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CryptoDbHelper cryptoDb = new CryptoDbHelper(ctx);

                TextView amtOwnedObj = (TextView) findViewById(R.id.amt_owned);
                TextView updatedAmtObj = (TextView) findViewById(R.id.update_amt);
                String updatedAmount = updatedAmtObj.getText().toString();

                if(updatedAmount == null || updatedAmount == "") {
                    updatedAmount = "0.00";
                } else if(updatedAmount.charAt(0) == '.') {
                    updatedAmount = "0" + updatedAmount;
                }

                if(updatedAmount.matches("\\d+(?:\\.\\d+)?")) {
                    BigDecimal updatedAmt = new BigDecimal(updatedAmount);
                    amtOwnedObj.setText(cryptoDb.setBitcoinCashAmount(updatedAmt).toString());

                    TextView valueOfAmtOwnedObj = (TextView) findViewById(R.id.value);
                    valueOfAmtOwnedObj.setText("$" + getValueOfAmountOwnedForBitcoinCash(ctx));
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                startActivity(new Intent(BitcoinCashActivity.this, MainActivity.class));
                return true;
            case R.id.menu_bitcoin:
                startActivity(new Intent(BitcoinCashActivity.this, BitcoinActivity.class));
                return true;
            case R.id.menu_bitcoin_cash:
                startActivity(new Intent(BitcoinCashActivity.this, BitcoinCashActivity.class));
                return true;
            case R.id.menu_ethereum:
                startActivity(new Intent(BitcoinCashActivity.this, EthereumActivity.class));
                return true;
            case R.id.menu_litecoin:
                startActivity(new Intent(BitcoinCashActivity.this, LitecoinActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
